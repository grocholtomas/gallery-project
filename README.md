Database setup in config.php.

# MySQL

``` sql

CREATE DATABASE gallery CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE users (
    UserID INT(11) NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(256) NOT NULL,
    last_name VARCHAR(256) NOT NULL,
    email VARCHAR(256) NOT NULL,
    username VARCHAR(256) NOT NULL,
    pwd VARCHAR(256) NOT NULL,
    PRIMARY KEY (UserID)
);

CREATE TABLE photos (
    PhotoID INT(11) NOT NULL AUTO_INCREMENT,
    UserID INT(11) NOT NULL,
    title VARCHAR(50) NOT NULL,
    pubdate VARCHAR(50) NOT NULL,
    image VARCHAR(255) NOT NULL,
    description VARCHAR(500) NOT NULL,    
    PRIMARY KEY (PhotoID),
    FOREIGN KEY (UserID) REFERENCES users (UserID)
);

CREATE TABLE comments (
    CommentID INT(11) NOT NULL AUTO_INCREMENT,
    UserID INT(11) NOT NULL,
    PhotoID INT(11) NOT NULL,
    ParentID INT(11) DEFAULT NULL,
    pubdate VARCHAR(50) NOT NULL,
    message VARCHAR(500) NOT NULL,
    PRIMARY KEY (CommentID),
    FOREIGN KEY (UserID) REFERENCES users (UserID),
    FOREIGN KEY (PhotoID) REFERENCES photos (PhotoID)
);

INSERT INTO photos (UserID, title, pubdate, image, description)
    VALUES ('1', 'Tree', '2018-08-10 21:11:20', 'f8d6e7ec9721df208c1826e045a7ecc0.jpg', 'Proin eleifend eros lectus, ac consectetur lorem maximus a. Nunc ullamcorper feugiat mauris nec suscipit. Quisque vel arcu commodo nunc vehicula ullamcorper a vitae mauris.'),
           ('1', 'Stone Ellipse', '2018-08-10 23:12:20', '64160316c8ada857a5001691deced367.jpg', 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris imperdiet sagittis justo eu consectetur. Aliquam pharetra lacus mi, quis sollicitudin sem lobortis at. Suspendisse ut nulla quis augue interdum egestas.'),
           ('1', 'Alaska Range', '2018-08-11 23:15:41', 'd93a15b949834ad1fa9ed2f331385dcf.jpg', 'The Alaska Range is a relatively narrow, 650-km-long (400 mi) mountain range in the southcentral region of the U.S. state of Alaska, from Lake Clark at its southwest end to the White River in Canada Yukon Territory in the southeast.'),
           ('1', 'The Scandinavian Mountains', '2018-08-11 23:11:41', '1f6faa6f9aa6266818af3b5bf7406c43.jpg', 'The Scandinavian Mountains or the Scandes is a mountain range that runs through the Scandinavian Peninsula. The Scandinavian Mountains are often erroneously thought to be equivalent to the Scandinavian Caledonides.'),
           ('1', 'Sunrise above Egelsee', '2018-08-11 21:11:41', '51a10c28660846d48544194c51eac159.jpg', 'Nam ut mauris sodales, auctor risus pharetra, egestas nibh. Nulla id consectetur quam. Cras tincidunt est a commodo finibus. Nullam nec quam nec justo tempor scelerisque.'),
           ('1', 'Lake Mountains', '2018-08-10 23:11:41', 'f80edd5865990aef509e7f7ce0066897.jpg', 'Quisque hendrerit mi justo, id ornare eros pulvinar eget. Pellentesque ultrices lobortis gravida. Pellentesque eget massa lacus. Fusce tempus volutpat lorem eu porttitor.'),
           ('1', 'Štrbské Pleso', '2018-08-12 23:20:41', '662652f2fa72863641f6f7f660353883.jpg', 'Štrbské Pleso is a favorite ski, tourist, and health resort in the High Tatras, Slovakia located on the lake by the same name. With extensive parking facilities and a stop on the Tatra trolley and rack railway, it is a starting point for a host of popular hikes including to Kriváň and Rysy.');

```