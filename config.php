<?php

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'gallery');

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_set_charset($conn, "utf8");
date_default_timezone_set('Europe/Vienna');

if (!$conn) {
	die("Connection failed: ".mysqli_connect_error());
}