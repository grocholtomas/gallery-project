<?php 
include 'includes/header.php';
include 'includes/comments.inc.php';

$cid = $_POST['CommentID'];
$photoid = $_POST['PhotoID'];
$message = $_POST['message'];
$currentuser = isset($_SESSION['username']) ? $_SESSION['username'] : '';

editComments($conn);

echo "<div class='container'>
		<form class='comment-form' method='POST'>			
			<input type='hidden' name='CommentID' value='".$cid."'>
			<input type='hidden' name='PhotoID' value='".$photoid."'>
			<div style='margin-top: 30px'class='form-group'>
				<div>
					<i class='fas fa-user'></i> ".$currentuser."
				</div>
				<br>
				<textarea maxlength='1000' class='form-control' name='message' required>".$message."</textarea>
				<br>
				<button style='float:right'type='submit' name='commentEdit' class='btn btn-warning'>Edit Comment</button>
			</div>
		</form>
	  </div>";

include 'includes/footer.php';