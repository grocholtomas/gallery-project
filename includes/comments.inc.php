<?php 

function setComments($conn) {
	if (isset($_POST['commentSubmit'])) {
		$uid = mysqli_escape_string($conn, $_POST['user_id']);
		$photoid = mysqli_escape_string($conn, $_GET['photoid']);
		$pubdate = mysqli_escape_string($conn, $_POST['pubdate']);
		$message = mysqli_escape_string($conn, $_POST['message']);	

		if (!empty($message)) {
			$sql = "INSERT INTO comments (UserID, PhotoID, pubdate, message) 
			VALUES ('$uid', '$photoid', '$pubdate', '$message')";
			$result = mysqli_query($conn, $sql);
		}	
	}
}


function getComments($conn) {
	$PhotoID = mysqli_escape_string($conn, $_GET['photoid']);
	$sqlc = "SELECT * FROM comments WHERE PhotoID='$PhotoID' ORDER BY pubdate ASC";
	$resultc = mysqli_query($conn, $sqlc);
	while ($rowc = mysqli_fetch_assoc($resultc)) {
		$UserID = $rowc['UserID'];
		$sqlu = "SELECT * FROM users WHERE UserID='$UserID'";
		$resultu = mysqli_query($conn, $sqlu);
		if ($rowu = mysqli_fetch_assoc($resultu)) {
			//Parent Comments
			if (is_null($rowc['ParentID'])) {
				echo "<div class='comment-box'>";
				echo "<i class='fas fa-user'></i> " . $rowu['username'] . '<br>';
				echo "<div class='pubdate'>" . $rowc['pubdate'] . '</div><br>';			
				echo nl2br($rowc['message']);
				echo "<br><br>";
				$ParentID = $rowc['CommentID'];
				$sql = "SELECT * FROM comments WHERE ParentID = '$ParentID' ORDER BY pubdate ASC";
				$result = mysqli_query($conn, $sql);
				//Reply Comments
				while ($row = mysqli_fetch_assoc($result)) {
					$UserIDr = $row['UserID'];
					$sqlr = "SELECT * FROM users WHERE UserID = '$UserIDr'";
					$resultr = mysqli_query($conn, $sqlr);
					$rowr = mysqli_fetch_assoc($resultr);
					echo "<div class='reply-box'>";
						echo "<i class='fas fa-user'></i> " . $rowr['username'] . '<br>';
						echo "<div class='pubdate'>" . $row['pubdate'] . '</div><br>';			
						echo nl2br($row['message']);
						//Buttons for Reply Comments
						if (isset($_SESSION['UserID'])) {
							if ($_SESSION['UserID'] == $rowr['UserID']) {
								deleteComments($conn);
								echo "<form class='delete-form' method='POST'>
										<input type='hidden' name='CommentID' value='".$row['CommentID']."'>
										<button type='submit' name='commentDelete' class='btn btn-outline-danger btn-sm'>
											Delete
										</button>
									  </form>
									  <form class='edit-form' method='POST' action='editcomment.php'>
										<input type='hidden' name='CommentID' value='".$row['CommentID']."'>
										<input type='hidden' name='PhotoID' value='".$row['PhotoID']."'>
										<input type='hidden' name='message' value='".$row['message']."'>
										<button class='btn btn-outline-warning btn-sm'>
											Edit
										</button>
									  </form>";
							}
						}
					echo "</div>";
				}
				//Buttons for Parent Comments
				if (isset($_SESSION['UserID'])) {
					//Buttons for owner of the Comment
					if ($_SESSION['UserID'] == $rowu['UserID']) {
						deleteComments($conn);
						echo "<form class='delete-form' method='POST'>
								<input type='hidden' name='CommentID' value='".$rowc['CommentID']."'>
								<button type='submit' name='commentDelete' class='btn btn-outline-danger btn-sm'>
									Delete
								</button>
							  </form>
							  <form class='edit-form' method='POST' action='editcomment.php'>
								<input type='hidden' name='CommentID' value='".$rowc['CommentID']."'>
								<input type='hidden' name='PhotoID' value='".$rowc['PhotoID']."'>
								<input type='hidden' name='message' value='".$rowc['message']."'>
								<button class='btn btn-outline-warning btn-sm'>
									Edit
								</button>
							  </form>
							  <form class='reply-self-form' method='POST' action='replycomment.php'>
								<input type='hidden' name='ParentID' value='".$rowc['CommentID']."'>
								<input type='hidden' name='PhotoID' value='".$rowc['PhotoID']."'>
								<input type='hidden' name='message' value='".$rowc['message']."'>								
								<input type='hidden' name='username' value='".$rowu['username']."'>
								<input type='hidden' name='pubdate' value='".$rowc['pubdate']."'>
								<button class='btn btn-outline-info btn-sm'>
									Reply
								</button>
							  </form>";
					} else {
						//Button for other users
						echo "<form class='reply-btn' method='POST' action='replycomment.php'>
								<input type='hidden' name='ParentID' value='".$rowc['CommentID']."'>
								<input type='hidden' name='PhotoID' value='".$rowc['PhotoID']."'>
								<input type='hidden' name='message' value='".$rowc['message']."'>								
								<input type='hidden' name='username' value='".$rowu['username']."'>							
								<input type='hidden' name='pubdate' value='".$rowc['pubdate']."'>
								<button class='btn btn-outline-info btn-sm'>
									Reply
								</button>
							  </form>";
					}
				}
				echo "</div><hr>";				
			}
		}
	}
}

function editComments($conn) {
	if (isset($_POST['commentEdit'])) {
		$cid = mysqli_escape_string($conn, $_POST['CommentID']);
		$photoid = mysqli_escape_string($conn, $_POST['PhotoID']);
		$pubdate = mysqli_escape_string($conn, date('Y-m-d H:i:s') . "<small> (Edited)</small> ");
		$message = mysqli_escape_string($conn, $_POST['message']);


		$sql = "UPDATE comments SET message='$message' , pubdate='$pubdate' WHERE CommentID='$cid'";
		$result = mysqli_query($conn, $sql);
		header('Location: photo.php?photoid=' . $photoid);
	}
}

function replyComment($conn) {
	if (isset($_POST['commentReply'])) {		
		$parent = mysqli_escape_string($conn, $_POST['ParentID']);
		$uid = mysqli_escape_string($conn, $_SESSION['UserID']);
		$photoid = mysqli_escape_string($conn, $_POST['PhotoID']);
		$pubdate = mysqli_escape_string($conn, date('Y-m-d H:i:s'));
		$message = mysqli_escape_string($conn, $_POST['replymessage']);

		$sql = "INSERT INTO comments (UserID, PhotoID, ParentID, pubdate, message) 
		VALUES ('$uid', '$photoid', '$parent', '$pubdate', '$message')";
		$result = mysqli_query($conn, $sql);
		header('Location: photo.php?photoid=' . $photoid);
	}
}


function deleteComments($conn) {
		if (isset($_POST['commentDelete'])) {
		$cid = mysqli_escape_string($conn, $_POST['CommentID']);

		$sql = "DELETE FROM comments WHERE CommentID='$cid' OR ParentID = '$cid'";
		$result = mysqli_query($conn, $sql);
		header("Refresh:0");
	}
}

function deletePhoto($conn) {
		if (isset($_POST['photoDelete'])) {
		$PhotoID = mysqli_escape_string($conn, $_POST['PhotoID']);
		$UserID = $_SESSION['UserID'];
		$sql = "SELECT * FROM photos WHERE PhotoID= '$PhotoID' AND UserID= $UserID";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($result);
		$filename = $row['image'];
		unlink('images/'.$filename);

		$sql = "DELETE FROM comments WHERE PhotoID='$PhotoID'";
		$result = mysqli_query($conn, $sql);
		$sql2 = "DELETE FROM photos WHERE PhotoID='$PhotoID' AND UserID= $UserID";
		$result2 = mysqli_query($conn, $sql2);
		$_SESSION['deletephoto'] = "Photo has been deleted.";
		header("Location: index.php");
	}
}