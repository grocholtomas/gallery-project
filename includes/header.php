<?php 
include 'config.php';
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Photo Gallery</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="style.css">	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="includes/javascript.js"></script>	
</head>
<body>
	<nav class="navbar navbar-dark bg-dark">
		<div class="container">
			<div class="left-menu">
				<a href="index.php"><i class="fas fa-home"></i> Home</a>
			</div>
			<div class="right-menu">
				<?php 
				//if Logged in
				if (isset($_SESSION['UserID'])) {
					echo '<div class="user">
							<form action="includes/logout.inc.php" method="POST">
								<a href="myphotos.php"><i class="fas fa-user"></i> '.$_SESSION["username"].'</a>
					 			<button type="submit" name="submit">
					 				Logout 
					 			<i class="fas fa-sign-out-alt"></i></button>
					 	 	</form>
			 	 		  </div>';
				} else {
				//if Logged off
					echo '<div class="sign-in"><a href="signup.php">
					 		Sign Up 
					 	  <i class="fas fa-user-plus"></i></a></div>
					 	  <div class="sign-in"><a href="signin.php">
					 	  	Sign In 
					 	  <i class="fas fa-sign-in-alt"></i></a></div>';
				}
				?>
			</div>
		</div>
	</nav>