<?php 
if (isset($_POST['submit'])) {
	session_start();
	session_unset();
	$_SESSION['logout'] = "See you later. <i class='far fa-smile'></i>";
	header("Location: ../index.php");
	exit();
}