<?php 

if (isset($_SESSION['signup'])){
	echo '<div class="success-handler">
				<div class="alert alert-success" role="alert">'.$_SESSION['signup'].'</div>
		  </div>';
	unset($_SESSION['signup']);
}

if (isset($_SESSION['login'])){
	echo '<div class="success-handler">
			  <div class="alert alert-success" role="alert">'.$_SESSION['login'].'</div>
		  </div>';
	unset($_SESSION['login']);
}

if (isset($_SESSION['upload'])){
	echo '<div class="success-handler">
			  <div class="alert alert-success" role="alert">'.$_SESSION['upload'].'</div>
		  </div>';
	unset($_SESSION['upload']);
}

if (isset($_SESSION['deletephoto'])){
	echo '<div class="success-handler">
			  <div class="alert alert-danger" role="alert">'.$_SESSION['deletephoto'].'</div>
		  </div>';
	unset($_SESSION['deletephoto']);
}

if (isset($_SESSION['logout'])){
	echo '<div class="success-handler">
			  <div class="alert alert-warning" role="alert">'.$_SESSION['logout'].'</div>
		  </div>';
	unset($_SESSION['logout']);
}