<?php 
include "includes/header.php";
?>

<div class="container">
	<div class="jumbotron">
	  	<h1 class="display-4">Photos from artists all over the world</h1>
	  	<hr class="my-4">
	  	<p>Share your photos with others and get your feedback</p>
	  	<a href="upload.php"><button class="btn btn-primary">Add new photo</button></a>
		<a href="index.php"><button class="btn btn-success">View all photos</button></a>	
	</div>
	<div class='card-deck'>
		<?php
		// define how many results you want per page
		$results_per_page = 3;
		// find out the number of results stored in database
		$UserID = $_SESSION['UserID'];
		$sql = "SELECT * FROM photos WHERE UserID = '$UserID'";
		$result = mysqli_query($conn, $sql);
		$number_of_results = mysqli_num_rows($result);
		// determine number of total pages available
		$number_of_pages = ceil($number_of_results/$results_per_page);
		// determine which page number visitor is currently on
		if (!isset($_GET['page'])) {
		  $page = 1;
		} else {
		  $page = $_GET['page'];
		}
		// determine the sql LIMIT starting number for the results on the displaying page
		$this_page_first_result = ($page-1)*$results_per_page;
		// retrieve selected results from database and display them on page
		$sql="SELECT * FROM photos WHERE UserID = '$UserID' LIMIT " . $this_page_first_result . "," .  $results_per_page;
		$result = mysqli_query($conn, $sql);
		while($row = mysqli_fetch_array($result)) {
		echo  "<div class='card'>
				<img class='card-img-top' src='images/".$row['image']."' alt='Card image cap'>
				<div class='card-body'>
					<h4>".$row['title']."</h4>
					<p class='card-text'>".$row['description']."</p>
				</div>
				<div class='card-footer'>
					<form method='GET' action='photo.php'>
					<input type='hidden' name='photoid' value='".$row['PhotoID']."'>
					<a href='photo.php'><button type='submit' class='btn btn-outline-info btn-sm'>
						Comments
					</button></a>
					</form>
				</div>
			  </div>";
		}
		?>
	</div>
	<nav aria-label="Page navigation example">
	  <ul class="pagination justify-content-center">
		<?php
		// display the links to the pages
		for ($page=1;$page<=$number_of_pages;$page++) {
		  echo '<li class="page-item"><a class="page-link" href="myphotos.php?page=' . $page . '">' . $page . '</a></li> ';
		}
		?>			
	  </ul>
	</nav>
</div>

<?php 
include "includes/footer.php" 
?>