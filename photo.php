<?php 
include 'includes/header.php';
include 'includes/comments.inc.php';

$photoID = $_GET['photoid'];
$sql = "SELECT * FROM photos WHERE PhotoID = '$photoID'";
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_array($result);
$UserID = $row['UserID'];
$sqlu = "SELECT * FROM users WHERE UserID = '$UserID'";
$resultu = mysqli_query($conn, $sqlu);
$rowu = mysqli_fetch_array($resultu);
?>

<div class="container">
    <div class="col-sm photo">
    	<img class="article-img" src="images/<?=$row["image"]?>">
    </div>
    <div class="delete-photo">
    	<?php 	
		if (isset($_SESSION['UserID'])) { 
    		$UserID = $_SESSION['UserID'];
			$sql3 = "SELECT * FROM photos WHERE PhotoID = '$photoID' AND UserID = '$UserID'";
			$result3 = mysqli_query($conn, $sql3);
			$row3 = mysqli_fetch_array($result3);						
	 		if ($UserID = $row3['UserID']) {
	 		deletePhoto($conn);
		  	echo "<form method='POST'>
					<input type='hidden' name='PhotoID' value='".$photoID."'>
					<button type='submit' name='photoDelete' class='btn btn-danger'>Delete Photo</button>
				  </form>";
			}
		}
		?>
	</div>
    <div class="col-sm photo">    	
    	<div class="title">
    		<h2><?=stripslashes($row["title"])?></h2>
    	</div>
    	<div class="pubdate float-right">
			<?=substr($row["pubdate"], 0, 16)?>
		</div>
    	<div>
			<p><i class='fas fa-user'></i> <?=$rowu["username"]?></p>				
		</div>
		<div class="description">
			<p><?=stripslashes($row["description"])?></p>				
		</div>
    </div>
	<hr class="style-six"></hr>

<?php
if (isset($_SESSION['UserID'])) {
	setComments($conn);			
	echo "<form class='comment-form' method='POST'>
			<input type='hidden' name='user_id' value='".$_SESSION['UserID']."'>
			<input type='hidden' name='pubdate' value='".date('Y-m-d H:i:s')."'>
			<div class='form-group'>
				<textarea maxlength='500' name='message'class='form-control' rows='3' placeholder='Write your comment...' required></textarea>
		  	</div>
			<button type='submit' name='commentSubmit' class='btn btn-outline-info float-right'>Comment</button>
		  </form>";
} else {
	echo "<form class='comment-form'>
			<div class='form-group'>
				<textarea name='message' class='form-control' id='comment' rows='3' placeholder='You must be logged in to comment' disabled></textarea>
		  	</div>
			<span title='You must be logged in to leave a comment'><button type='button' class='btn btn-outline-info float-right disabled'>Comment</button></span>
		  </form>";
}

echo "<div class='comment-section'><hr>";
getComments($conn);
echo "</div>";

include 'includes/footer.php'
?>