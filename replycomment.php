<?php 
include 'includes/header.php';
include 'includes/comments.inc.php';

$parent = isset($_POST['ParentID']) ? $_POST['ParentID'] : '';
$photoid = isset($_POST['PhotoID']) ? $_POST['PhotoID'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';
$username = isset($_POST['username']) ? $_POST['username'] : ''; 
$pubdate = isset($_POST['pubdate']) ? $_POST['pubdate'] : '';
$currentuser = isset($_SESSION['username']) ? $_SESSION['username'] : '';

replyComment($conn);

echo "<div class='container'>
		<hr>
		<div class='comment-box'>							
			<i class='fas fa-user'></i> ".$username."<br>
			<div class='pubdate'>".$pubdate."</div><br>			
			".$message."	
		</div>
		<hr>
		<form class='comment-form' method='POST'>
			<input type='hidden' name='ParentID' value='".$parent."'>
			<input type='hidden' name='PhotoID' value='".$photoid."'>
			<div class='form-group'>
				<div class='reply-form'>
					<div>
						<i class='fas fa-user'></i> ".$currentuser."
					</div>
					<br>
					<textarea maxlength='1000' class='form-control' name='replymessage' required></textarea>
					<br>
					<button style='float:right' type='submit' name='commentReply' class='btn btn-info'>
						Reply
					</button>
				</div>
			</div>
		</form>
	  </div>";

include 'includes/footer.php';