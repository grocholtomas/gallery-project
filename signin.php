<?php 
include 'includes/header.php';

if (isset($_POST['user_name'])) {
	
	$uid = mysqli_real_escape_string($conn, $_POST['user_name']);
	$pwd = mysqli_real_escape_string($conn, $_POST['user_pwd']);

		$required = [
		'user_name' => 'Username',
		'user_pwd' => 'Password'
	];

	$error = [];
	foreach ($required as $name => $label) {
		if (empty($_POST[$name])) {
			$error[$name] = $label . ' cannot be empty.';
		}
	}

	if (empty($error)) {

		$sql = "SELECT * FROM users WHERE username= '$uid' OR email= '$uid'";
		$result = mysqli_query($conn, $sql);
		$resultCheck = mysqli_num_rows($result);
		if ($resultCheck < 1) {
			$error = "Wrong Username or Password.";
			echo "<div class='error-handler'>
			 		<div class='alert alert-danger' role='alert'>"
					.$error.
					"</div>
				  </div>";
		} else { 	
			if ($row = mysqli_fetch_assoc($result)) {
				//De-hashing the password
				$hashedPwdCheck = password_verify($pwd, $row['pwd']);
				if ($hashedPwdCheck == false) {
					$error = "Wrong Username or Password.";
					echo "<div class='error-handler'>
					 		<div class='alert alert-danger' role='alert'>"
							.$error.
							"</div>
						  </div>";
				} elseif ($hashedPwdCheck == true) {
					//Log in the user here
					$_SESSION['UserID'] = $row['UserID'];
					$_SESSION['first_name'] = $row['first_name'];
					$_SESSION['last_name'] = $row['last_name'];
					$_SESSION['email'] = $row['email'];
					$_SESSION['username'] = $row['username'];
					$_SESSION['login'] = "Welcome back " . $row['first_name'] . "! <i class='far fa-smile'></i>";
					header("Location: index.php");
					exit();
				}
			}
		}
	} else {
		echo "<div class='error-handler'>";
			foreach ($error as $err) {
				echo '<div class="alert alert-danger" role="alert">'.$err.'</div>';
			}
		echo "</div>";
	}

	if (isset($_SESSION['signup'])){
	echo '<div class="success-handler">
				<div class="alert alert-primary" role="alert">'.$_SESSION['signup'].'</div>
			</div>';
		unset($_SESSION['signup']);
	}
}

 ?>

<form class="login-form" action="signin.php" method="POST">
	<div class="form-group">
		<h1>Sign In</h1>
		<br>
	</div>
	<div class="form-group">
		<input type="text" name="user_name" class="form-control" aria-describedby="emailHelp" placeholder="Username or E-mail" value="<?=(!empty($_POST['user_name']) ? $_POST['user_name'] : '')?>">
	</div>
	<div class="form-group">
		<input type="password" name="user_pwd" class="form-control"  placeholder="Password" value="<?=(!empty($_POST['user_pwd']) ? $_POST['user_pwd'] : '')?>">
	</div>
	<button type="submit" name="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Sign In</button>
</form>

<?php
include 'includes/footer.php'
 ?>