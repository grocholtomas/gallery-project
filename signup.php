<?php 
include 'includes/header.php';

if (isset($_POST['user_first'])) {
	
	include_once 'config.php';

	$first = mysqli_real_escape_string($conn, $_POST['user_first']);
	$last = mysqli_real_escape_string($conn, $_POST['user_last']);
	$email = mysqli_real_escape_string($conn, $_POST['user_email']);
	$uid = mysqli_real_escape_string($conn, $_POST['user_name']);
	$pwd = mysqli_real_escape_string($conn, $_POST['user_pwd']);

	//Error handlers
	//Check for empty fields

	$required = [
		'user_first' => 'First name',
		'user_last' => 'Last name',
		'user_email' => 'Email',
		'user_name' => 'Username',
		'user_pwd' => 'Password'
	];

	$error = [];
	foreach ($required as $name => $label) {
		if (empty($_POST[$name])) {
			$error[$name] = $label . ' cannot be empty.';
		}
	}

	if (empty($error)) {

		//Check if email is valid
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$sqlreg = "SELECT * FROM users WHERE username='$uid'";
			$resultreg = mysqli_query($conn, $sqlreg);
			$resultCheck = mysqli_num_rows($resultreg);
			if ($resultCheck == 0) {
				//Hashing the password
				$hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);
				//Insert the user into the database
				$sql = "INSERT INTO users (first_name, last_name, email, username, pwd) VALUES ('$first', '$last', '$email', '$uid', '$hashedPwd');";
				mysqli_query($conn, $sql);
				//Log in the user here
				$sqllog = "SELECT * FROM users WHERE username= '$uid'";
				$resultlog = mysqli_query($conn, $sqllog);
					if ($row = mysqli_fetch_assoc($resultlog)) {
						$_SESSION['UserID'] = $row['UserID'];
						$_SESSION['first_name'] = $row['first_name'];
						$_SESSION['last_name'] = $row['last_name'];
						$_SESSION['email'] = $row['email'];
						$_SESSION['username'] = $row['username'];
						$_SESSION['signup'] = "Welcome " . $row['first_name'] . "! <i class='far fa-smile'></i>";
						header("Location: index.php");
						exit();
					}
			} else {
				$error['user_name'] = "Username already exists";
			}
		}
	}
}

if (!empty($error)) {
	echo "<div class='error-handler'>";
		foreach ($error as $err) {
			echo '<div class="alert alert-danger" role="alert">'.$err.'</div>';
		}
	echo "</div>";
}
?>

<form class="register-form" action="signup.php" method="POST" required>
	<div class="form-group">
		<h1>Sign Up</h1>
		<br>
	</div>
	<div class="form-group">
		<input type="text" name="user_first" value="<?=(!empty($_POST['user_first']) ? $_POST['user_first'] : '')?>" class="form-control" placeholder="First Name" >
	</div>
	<div class="form-group">
		<input type="text" name="user_last" value="<?=(!empty($_POST['user_last']) ? $_POST['user_last'] : '')?>" class="form-control"  placeholder="Last Name" >
	</div>
	<div class="form-group">
		<input type="email" name="user_email" value="<?=(!empty($_POST['user_email']) ? $_POST['user_email'] : '')?>" class="form-control"  placeholder="E-mail" >
	</div>
	<div class="form-group">
		<input type="text" name="user_name" value="<?=(!empty($_POST['user_name']) ? $_POST['user_name'] : '')?>" class="form-control"  placeholder="Username" >
	</div>
	<div class="form-group">
		<input type="password" name="user_pwd" class="form-control"  placeholder="Password" >
	</div>
	<button type="submit" name="submit" class="btn btn-primary">Sign Up <i class="fas fa-user-plus"></i></button>
</form>

<?php 
include 'includes/footer.php'
?>