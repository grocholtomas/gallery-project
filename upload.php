<?php 
include 'includes/header.php';
include 'includes/diacritics.inc.php';

// if upload button is pressed
if (isset($_POST['upload'])) {	
	$title = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['title']));
	$pubdate = date('Y-m-d H:i:s');
	$time = time();
	$description = htmlspecialchars(mysqli_real_escape_string($conn, $_POST['description']));

	//accept only these file extensions
	$imageMime = $_FILES['image']['type'];		
	$fileExtensions = [
		"image/jpg" => ".jpg",
		"image/jpeg" => ".jpg",
		"image/png" => ".png"
	];
	//create name for image with file extension
	$image = md5($time . uniqid()) . $fileExtensions[$imageMime];
	//path to store the uploaded image
	$target = "images/". $image;

	//store the submitted data into the database table photos
	$required = [
	'title' => 'Title',
	'description' => 'Description'
	];

	$error = [];
	foreach ($required as $name => $label) {
		if (empty($_POST[$name])) {
			$error[$name] = $label . ' cannot be empty.';
		}
	}		
	if (empty($error)) {
		if(isset($_SESSION['UserID'])){
			$UserID = $_SESSION['UserID'];
			$sql = "INSERT INTO photos (UserID, title, pubdate, image, description) 
			VALUES ('$UserID', '$title', '$pubdate', '$image', '$description')";
			mysqli_query($conn, $sql); 

			// move the uploaded image into the folder: images
			if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {	
				$_SESSION['upload'] = "You have uploaded new photo.";
				header("Location: index.php");
				exit();
			}
		}
		echo "<div class='error-handler'>";
			echo '<div class="alert alert-danger" role="alert">You must be logged in to do that</div>';
		echo "</div>";
	}
	echo "<div class='error-handler'>";
		foreach ($error as $err) {
			echo '<div class="alert alert-danger" role="alert">'.$err.'</div>';
		}
	echo "</div>";
}

 ?>

<div class="container">	
	<div class="upload-form">
		<form method="POST" action="upload.php" enctype="multipart/form-data">
			<input type="hidden" name="size" value="1000000">
			<input type="hidden" name="pubdate" value="<?=date('Y-m-d H:i:s')?>">
			<div class="custom-file">
				<input type="file" name="image" required>
			</div>
			<div class="img-title">
				<input type="text" class="form-control" name="title" placeholder="Image title" value="<?=(!empty($_POST['title']) ? $_POST['title'] : '')?>">
			</div>
			<div class="img-desc">
				<textarea maxlength="500" name="description" class="form-control" cols="40" rows="4" placeholder="Say something about this image..." value="<?=(!empty($_POST['description']) ? $_POST['description'] : '')?>"></textarea>
			</div>
			<div class="upload">
				<button class="btn btn-primary" type="submit" name="upload">Upload</button>
			</div>
		</form>
	</div>
</div>

<?php 
include 'includes/footer.php'
?>